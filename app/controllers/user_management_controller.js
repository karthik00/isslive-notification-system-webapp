load('application');

action('index', function(){
	if(req.session.user==null){
		redirect('/login')
	}
	else{
		redirect('/filters')
	}
})

action('login', function () {
	this.user=new User
	render({title: "ISSLive Notification system"});
});

action('chk_login', function () {
	User.find(req.body.User, function(err, docs){
		//console.log(docs)
		if(docs.length==1){
			req.session.user=docs[0]
			redirect('/filters')
		}
		else{
			redirect('/login')
		}
	});
});

action('logout', function(){
	req.session.user=null;
	redirect('/');
})

action('signup', function(){
	this.user=new User
	render({title: "ISSLive Notification system"});
})

action('register', function(){
	User.find({userName:req.body.User.userName}, function(err, docs){
		if(docs.length>0){
			flash('error', 'The username already exists');
			redirect('/signup')
		}
		else{
			user=new User(req.body.User)
			user.save(function(err){
				if(err){
					flash('error', err);
					redirect('/signup')
				}
				else{
					session.user=user
					redirect('/filters')
				}
			});
		}
	});
})