before('protect from forgery', function () {
    protectFromForgery('46befc73c421589ef5009911e3eb52c83e037946');
});


action('index', function () {
	if(req.session.user==null){
		redirect('/login')
	}
	else{
		redirect('/filters')
	}
});