load('application');
var client=require('../../redisio').client

before(function(){
	if(req.session.user==null){
		redirect('/login')
	}
	else{
		this.current_user=req.session.user
	}
	next();
});

action('active_filters', function () {
	Filter.find({isActive:true, user:this.current_user._id}, function(err,docs){
		if(err)throw err
		render({title: "ISSLive Notification system", current_page: 'active_filters', user_filters:docs});
	})
});

action('all_filters', function () {
	Filter.find({user:this.current_user._id}, function(err,docs){
		if(err)throw err
		render({title: "ISSLive Notification system", current_page: 'all_filters', user_filters:docs});
	})
});

action('new_filter', function () {
	this.filter=new Filter
	render({title: "ISSLive Notification system", current_page: 'new_filter'});
});

action('create_new_filter', function(){
	var current_user=this.current_user
	Filter.find({user:this.current_user._id, filterName:req.body.Filter.filterName}, function(err, docs){
		if(err) throw err
		if(docs.length>0){
			flash('error', 'The filter name already exists.')
			redirect('/filters/new_filter')
		}
		else{
			filter=new Filter(req.body.Filter)
			filter.user=current_user._id
			filter.save(function(err){
				if(err){
					flash('error', err);
					redirect('/filters/new_filter')
				}
				else{
					addFilterToRedis(filter)
					redirect('/filters')
				}
			})
		}
	})
})

action('edit_filter', function () {
	Filter.findById(req.params.filter_id, function(err, doc){
		if(err){
			flash("error", err)
			render('/filters/all_filters');
		}
		else{
			render({title: "ISSLive Notification system", filter:doc, current_page:'edit_filter'});
		}
	})
});

action('notification_settings', function () {
	User.findById(this.current_user._id, function(err,doc){
		if(err)throw err
		render({title: "ISSLive Notification system", current_page: 'notification_settings', notification_settings:doc.notificationSettings});
	})
});

action('save_notification_settings', function () {
	User.findById(this.current_user._id, function(err, user){
		if(err) {
			flash("error", err)
			redirect('/filters/notification_settings')
			return
		}
		user.notificationSettings=req.body.User.notificationSettings
		user.save(function(err){
			if(err){
				flash('error', err)
			}
			redirect('/filters/notification_settings')
		})
	})
});

action('save_filter', function () {
	Filter.findById(req.params.filter_id, function(err, doc){
		if(err)throw err
		//oldFil=clone(doc)
		Filter.update({_id:req.params.filter_id}, req.body.Filter, {}, function(err){
			if(err){
				flash('error', err)
				redirect('/filters/all_filters')
			}
			else{
				newFil=req.body.Filter
				newFil._id=doc._id
				removeFiterFromRedis(doc)
				addFilterToRedis(newFil)
				redirect('/filters/all_filters')
			}
		})
	});
});

action('delete_filter', function () {
	current_user=this.current_user;
	Filter.findById(req.params.filter_id, function(err, doc){
		if(err)throw err
		doc.remove(function(err){
			if(err){
				flash('error', err)
				redirect('/filters/edit_filter/'+doc._id)
			}
			else{
				removeFiterFromRedis(doc)
				redirect('/filters/all_filters')
			}
		})
	});
});

action('activate_filter', function () {
	Filter.findById(req.params.filter_id, function(err, doc){
		if(err){
			send({result:'falilure'})
		}
		if(doc){
			doc.isActive=req.params.value=='true';
		}
		doc.save(function(err){
			if(err)
				send({result:'failure'})
			else{
				if(req.params.value=='true'){
					addFilterToRedis(doc)
				}
				else{
					removeFiterFromRedis(doc)
				}
				send({result:'success'})
			}
		});
	})
});


action('get_filter_value', function(){
	Filter.findById(req.params.filter_id, function(err, doc){
		if(err){
			send(err)
		}
		if(doc){
			send(doc)
		}
	})
})

var addFilterToRedis= function(filter){
	client.zadd(filter.sensorName, filter.upperLimit==null ? "+inf":filter.upperLimit, "u"+filter._id)
	client.zadd(filter.sensorName, filter.lowerLimit==null ? "-inf":filter.lowerLimit, "l"+filter._id)
}

var removeFiterFromRedis= function(filter){
	client.zrem(filter.sensorName, filter.upperLimit==null ? "+inf":filter.upperLimit, "u"+filter._id)
	client.zrem(filter.sensorName, filter.lowerLimit==null ? "-inf":filter.lowerLimit, "l"+filter._id)
}