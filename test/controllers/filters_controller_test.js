require('../test_helper.js').controller('filters', module.exports);

var sinon  = require('sinon');

function ValidAttributes () {
    return {
        filter_name: '',
        sensor: '',
        upper_limit: '',
        lower_limit: '',
        last_notified: '',
        createdAt: ''
    };
}

exports['filters controller'] = {

    'GET new': function (test) {
        test.get('/filters/new', function () {
            test.success();
            test.render('new');
            test.render('form.' + app.set('view engine'));
            test.done();
        });
    },

    'GET index': function (test) {
        test.get('/filters', function () {
            test.success();
            test.render('index');
            test.done();
        });
    },

    'GET edit': function (test) {
        var find = Filter.find;
        Filter.find = sinon.spy(function (id, callback) {
            callback(null, new Filter);
        });
        test.get('/filters/42/edit', function () {
            test.ok(Filter.find.calledWith('42'));
            Filter.find = find;
            test.success();
            test.render('edit');
            test.done();
        });
    },

    'GET show': function (test) {
        var find = Filter.find;
        Filter.find = sinon.spy(function (id, callback) {
            callback(null, new Filter);
        });
        test.get('/filters/42', function (req, res) {
            test.ok(Filter.find.calledWith('42'));
            Filter.find = find;
            test.success();
            test.render('show');
            test.done();
        });
    },

    'POST create': function (test) {
        var filter = new ValidAttributes;
        var create = Filter.create;
        Filter.create = sinon.spy(function (data, callback) {
            test.strictEqual(data, filter);
            callback(null, filter);
        });
        test.post('/filters', {Filter: filter}, function () {
            test.redirect('/filters');
            test.flash('info');
            test.done();
        });
    },

    'POST create fail': function (test) {
        var filter = new ValidAttributes;
        var create = Filter.create;
        Filter.create = sinon.spy(function (data, callback) {
            test.strictEqual(data, filter);
            callback(new Error, filter);
        });
        test.post('/filters', {Filter: filter}, function () {
            test.success();
            test.render('new');
            test.flash('error');
            test.done();
        });
    },

    'PUT update': function (test) {
        Filter.find = sinon.spy(function (id, callback) {
            test.equal(id, 1);
            callback(null, {id: 1, updateAttributes: function (data, cb) { cb(null); }});
        });
        test.put('/filters/1', new ValidAttributes, function () {
            test.redirect('/filters/1');
            test.flash('info');
            test.done();
        });
    },

    'PUT update fail': function (test) {
        Filter.find = sinon.spy(function (id, callback) {
            test.equal(id, 1);
            callback(null, {id: 1, updateAttributes: function (data, cb) { cb(new Error); }});
        });
        test.put('/filters/1', new ValidAttributes, function () {
            test.success();
            test.render('edit');
            test.flash('error');
            test.done();
        });
    },

    'DELETE destroy': function (test) {
        test.done();
    },

    'DELETE destroy fail': function (test) {
        test.done();
    }
};

