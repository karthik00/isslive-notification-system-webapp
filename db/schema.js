/*
db/schema.js contains database schema description for application models
by default (when using jugglingdb as ORM) this file uses database connection
described in config/database.json. But it's possible to use another database
connections and multiple different schemas, docs available at

http://railwayjs.com/orm.html

Example of model definition:

define('User', function () {
property('email', String, { index: true });
property('password', String);
property('activated', Boolean, {default: false});
});

Example of schema configured without config/database.json (heroku redistogo addon):
schema('redis', {url: process.env.REDISTOGO_URL}, function () {
// model definitions here
});

*/
customSchema(function() {
    var mongoose = require('mongoose');
    mongoose.connect('mongodb://localhost/iss_notification_system');

    var Schema = mongoose.Schema;
	var ObjectId = Schema.ObjectId;

    var Filter = new Schema({
        filterName: String,
		sensorName: String,
        customMessage: String,
        upperLimit: Number,
        lowerLimit: Number,
        createdDate: Date,
        lastNotifiedDate: Date,
        isActive: Boolean,
		currentValue: Number,
		status: String,
		user:ObjectId
    });

    var User = new Schema({
        userName: String,
        password: String,
        date: Date,
        notificationSettings: {
			notificationPingPath: String
        }
    });

    var _User = mongoose.model('User', User);
    _User.modelName = 'User';
    module.exports['User'] = _User;

    var _Filter = mongoose.model('Filter', Filter);
	_Filter.modelName = 'Filter';
	module.exports['Filter'] = _Filter;

    // module.exports['Filter'] = function() {
    //     return {}
    // };

});