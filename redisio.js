var redis = require("redis")
var qs = require('querystring');
var http = require("http")
var pub_sub_client = redis.createClient();
var client= this.client=redis.createClient();
this.client.flushdb();
this.client.flushall();

Filter.find({isActive:true}, function(err, docs){
	if(err) throw err
	docs.forEach(function(item){
		addFilterToRedis(item);
	});
})

this.listen=function(){
	pub_sub_client.on("message", function (channel, message) {
		result=JSON.parse(message);
						if(result.status=='in'){
													Filter.findById(result.filter.substr(1), function(err, doc){
														if(err) throw err
														if(doc){
															
															var querystring = require('querystring');
															var http = require('http');

															var post_domain = 'localhost';
															var post_port = 1337;
															var post_path = '/post_demo';

															var post_data = JSON.stringify(doc);

															var post_options = {
															  host: post_domain,
															  port: post_port,
															  path: post_path,
															  method: 'POST',
															  headers: {
															    'Content-Type': 'application/x-www-form-urlencoded',
															    'Content-Length': post_data.length
															  }
															};

															var post_req = http.request(post_options, function(res) {
															  res.setEncoding('utf8');
															  res.on('data', function (chunk) {
															    console.log('Response: ' + chunk);
															  });
															});

															// write parameters to post body
															post_req.write(post_data);
															post_req.end();
															
															
															
															doc.currentValue=result.value
															doc.status="normal"
															doc.save(function(err){})
														}
													})
												}
												else{
													Filter.findById(result.filter.substr(1), function(err, doc){
														if(err) throw err
														if(doc){
															doc.currentValue=result.value
															doc.status="out"
															doc.save(function(err){})
														}
													})
												}
	});
	pub_sub_client.subscribe("filter_notify_channel");
}

var addFilterToRedis= function(filter){
	client.zadd(filter.sensorName, filter.upperLimit==null ? "+inf":filter.upperLimit, "u"+filter._id)
	client.zadd(filter.sensorName, filter.lowerLimit==null ? "-inf":filter.lowerLimit, "l"+filter._id)
}